use bzip2::read::BzDecoder;
use failure::{bail, err_msg, format_err, Error, ResultExt};
use log::{error, info};
use scraper::{Html, Selector};
#[cfg(unix)]
use std::os::unix::fs::PermissionsExt;
use std::{
    fs,
    io::{self, prelude::*},
    path::{Path, PathBuf},
    process::{self, Command},
    str::{self, FromStr},
};
use structopt::StructOpt;

macro_rules! base_url {
    () => {
        "https://builder.blender.org/"
    };
}
static BASE_URL: &str = base_url!();
static DOWNLOAD_PAGE_URL: &str = concat!(base_url!(), "download/");

type FResult<T = ()> = Result<T, failure::Error>;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum Arch {
    X86,
    X64,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum Platform {
    Linux(Arch),
    Windows(Arch),
    MacOS,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum Version {
    Official,
    V28,
}

impl FromStr for Version {
    type Err = Error;

    fn from_str(s: &str) -> FResult<Version> {
        Ok(match s.to_lowercase().as_ref() {
            "official" => Version::Official,
            "2.8" => Version::V28,
            _ => bail!("Unsupported version string. Accepted values are: [official, 2.8]"),
        })
    }
}

impl Platform {
    #[allow(unreachable_code)]
    pub fn current() -> Option<Platform> {
        #[cfg(all(target_os = "windows", target_arch = "x86"))]
        return Some(Platform::Windows(Arch::X86));
        #[cfg(all(target_os = "windows", target_arch = "x86_64"))]
        return Some(Platform::Windows(Arch::X64));
        #[cfg(all(target_os = "linux", target_arch = "x86"))]
        return Some(Platform::Linux(Arch::X86));
        #[cfg(all(target_os = "linux", target_arch = "x86_64"))]
        return Some(Platform::Linux(Arch::X64));
        #[cfg(target_os = "macos")]
        return Some(Platform::MacOS);
        None
    }

    pub fn default_version(&self) -> Version {
        match self {
            Platform::Linux(arch) => match arch {
                Arch::X86 => Version::Official,
                Arch::X64 => Version::Official,
            },
            Platform::Windows(arch) => match arch {
                Arch::X86 => Version::Official,
                Arch::X64 => Version::Official,
            },
            Platform::MacOS => Version::Official,
        }
    }

    pub fn download_selector(&self, version: Version) -> Option<String> {
        let selector = |os, item| format!("li.{}:nth-child({}) > a:nth-child(1)", os, item,);

        match self {
            Platform::Windows(arch) => match arch {
                Arch::X64 => match version {
                    Version::Official => Some(selector("windows", 3)),
                    Version::V28 => Some(selector("windows", 1)),
                },
                Arch::X86 => match version {
                    Version::Official => Some(selector("windows", 4)),
                    Version::V28 => Some(selector("windows", 2)),
                },
            },
            Platform::Linux(arch) => match arch {
                Arch::X64 => match version {
                    Version::Official => Some(selector("linux", 3)),
                    Version::V28 => Some(selector("linux", 1)),
                },
                Arch::X86 => match version {
                    Version::Official => Some(selector("linux", 4)),
                    Version::V28 => Some(selector("linux", 2)),
                },
            },
            Platform::MacOS => match version {
                Version::Official => Some(selector("macos", 2)),
                Version::V28 => Some(selector("macos", 1)),
            },
        }
    }
}

impl FromStr for Platform {
    type Err = Error;

    fn from_str(s: &str) -> FResult<Platform> {
        Ok(match s.to_lowercase().as_ref() {
            "win32" => Platform::Windows(Arch::X86),
            "win64" => Platform::Windows(Arch::X64),
            "linux32" => Platform::Linux(Arch::X86),
            "linux64" => Platform::Linux(Arch::X64),
            "macos" => Platform::MacOS,
            _ => bail!("Unsupported platform string. Accepted values are: [win32, win64, linux32, linux64, macos]"),
        })
    }
}

fn extract_archive(platform: Platform, response: &mut reqwest::Response, path: &Path) -> FResult {
    let _ = fs::remove_dir_all(path);
    fs::create_dir(path).context("Couldn't create Blender installation dir.")?;
    let stdout = io::stdout();
    let mut out = stdout.lock();

    match platform {
        Platform::Linux(_) => {
            let mut archive = tar::Archive::new(BzDecoder::new(response));
            for entry in archive.entries()? {
                let mut entry = entry?;
                let target_path = path.join(
                    entry
                        .path()
                        .context("Encountered an invalid path in Blender archive.")?
                        .components()
                        .skip(1)
                        .collect::<PathBuf>(),
                );
                let _ = writeln!(&mut out, "Extracting to: {}", target_path.to_string_lossy());
                match target_path.parent() {
                    Some(parent) if !parent.is_dir() => {
                        fs::create_dir_all(parent).context("Couldn't create entry directory.")?
                    }
                    _ => {}
                }
                let mut target_file =
                    fs::File::create(&target_path).context("Couldn't create entry file.")?;
                io::copy(&mut entry, &mut target_file).context("Couldn't extract entry.")?;
                #[cfg(unix)]
                {
                    if let Ok(mode) = entry.header().mode() {
                        if target_file
                            .set_permissions(fs::Permissions::from_mode(mode))
                            .is_err()
                        {
                            error!("Failed to set permissions for: {:?}", target_path);
                        }
                    }
                }
            }
        }
        _ => {
            let mut archive =
                tempfile::tempfile().context("Unable to create temporary archive file.")?;
            response
                .copy_to(&mut archive)
                .context("IO error while downloading Blender.")?;
            archive.seek(io::SeekFrom::Current(0))?;

            let mut archive = zip::ZipArchive::new(archive)?;
            for entry_i in 0..archive.len() {
                let mut entry = archive
                    .by_index(entry_i)
                    .context("Unable to read file from Blender archive.")?;
                // Skip directories
                if entry.size() == 0 {
                    continue;
                }
                let target_path = path.join(
                    entry
                        .sanitized_name()
                        .components()
                        .skip(1)
                        .collect::<PathBuf>(),
                );
                let _ = writeln!(&mut out, "Extracting to: {}", target_path.to_string_lossy());
                match target_path.parent() {
                    Some(parent) if !parent.is_dir() => {
                        fs::create_dir_all(parent).context("Couldn't create entry directory.")?
                    }
                    _ => {}
                }
                let mut target_file =
                    fs::File::create(&target_path).context("Couldn't create entry file.")?;
                io::copy(&mut entry, &mut target_file).context("Couldn't extract entry.")?;
                #[cfg(unix)]
                {
                    if let Some(mode) = entry.unix_mode() {
                        if target_file
                            .set_permissions(fs::Permissions::from_mode(mode))
                            .is_err()
                        {
                            error!("Failed to set permissions for: {:?}", target_path);
                        }
                    }
                }
            }
        }
    }

    Ok(())
}

#[derive(Debug, StructOpt)]
#[structopt(author = "")]
struct Opt {
    /// The Blender installation path.
    #[structopt(name = "PATH", parse(from_os_str))]
    path: PathBuf,
    /// The target platform (OS and arch). Defaults to the current platform.
    ///
    /// Accepted values are: [win32, win64, linux32, linux64, macos]
    #[structopt(short = "p", long = "platform", parse(try_from_str))]
    platform: Option<Platform>,
    /// The version/type of Blender to download. Defaults to official.
    ///
    /// Accepted values are: [official, 2.8]
    #[structopt(short = "t", long = "type", parse(try_from_str))]
    ty: Option<Version>,
    /// Skips the version check and forces installation.
    #[structopt(short = "f", long = "force")]
    force: bool,
    /// Pass many times for more log output
    ///
    /// By default, it'll report info. Passing `-v` one time also prints
    /// debug, `-vv` enables trace.
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    verbose: u8,
    /// Disables info log output, only displaying errors. Overrides --verbose.
    #[structopt(long = "silent")]
    silent: bool,
}

fn installation_build_hash<P: AsRef<Path>>(path: P) -> Option<String> {
    let output = Command::new(path.as_ref()).arg("--version").output().ok()?;
    let stdout = str::from_utf8(&output.stdout).ok()?;
    stdout
        .lines()
        .filter_map(|line| {
            line.find("build hash")
                .and_then(|i| line.get(i + 12..i + 23))
        })
        .next()
        .map(String::from)
}

fn url_build_hash<P: AsRef<Path>>(url: P) -> Option<String> {
    let url = url.as_ref();
    let file_name = url.file_stem()?.to_string_lossy();
    let mut dashes = file_name.match_indices('-');
    dashes.next()?;
    let start = dashes.next()?.0 + 1;
    let end = dashes.next()?.0;
    file_name.get(start..end).map(String::from)
}

fn run() -> FResult {
    let opt: Opt = Opt::from_args();

    let log_level = if opt.silent {
        simplelog::LevelFilter::Error
    } else {
        match opt.verbose {
            0 => simplelog::LevelFilter::Info,
            1 => simplelog::LevelFilter::Debug,
            _ => simplelog::LevelFilter::Trace,
        }
    };
    simplelog::TermLogger::init(log_level, Default::default())?;

    let platform = match opt.platform {
        Some(p) => p,
        None => Platform::current().ok_or_else(|| err_msg("Running on unsupported platform."))?,
    };
    let version = opt.ty.unwrap_or_else(|| platform.default_version());

    let selector = platform.download_selector(version).ok_or_else(|| {
        format_err!(
            "Blender version {:?} not supported for platform {:?}.",
            version,
            platform
        )
    })?;

    let mut page = reqwest::get(DOWNLOAD_PAGE_URL).context("Failed to fetch download page.")?;
    let body = Html::parse_document(&page.text().context("Download page invalid.")?);
    let download_suffix = body
        .select(&Selector::parse(&selector).expect(&format!("Invalid selector: {}", selector)))
        .next()
        .ok_or_else(|| err_msg("No download link element found."))?
        .value()
        .attr("href")
        .ok_or_else(|| err_msg("No download link found."))?;
    let download_link = BASE_URL.to_string() + download_suffix;

    if !opt.force && platform != Platform::MacOS {
        let download_build_hash = url_build_hash(&download_suffix);
        let installation_build_hash = installation_build_hash(opt.path.join("blender"));

        if let (Some(download_hash), Some(installation_hash)) =
            (download_build_hash, installation_build_hash)
        {
            if download_hash == installation_hash {
                info!("Installation is already up to date ({}).", download_hash);
                info!("Done");
                return Ok(());
            }
        }
    }

    info!(
        "Downloading Blender {:?} for {:?} from\n\t{} ...",
        version, platform, download_link
    );
    let mut response = reqwest::get(&download_link)?;
    if !response.status().is_success() {
        Err(format_err!(
            "Download failed: {:?}",
            response
                .status()
                .canonical_reason()
                .unwrap_or("Unknown reason")
        ))?;
    }

    extract_archive(platform, &mut response, &opt.path)?;

    info!("Done");
    Ok(())
}

fn main() {
    if let Err(e) = run() {
        let mut causes = e.iter_chain();
        error!("Error: {}", causes.next().unwrap());
        for cause in causes {
            error!("Cause: {}", cause);
        }
        process::exit(
            if let Some(code) = e
                .find_root_cause()
                .downcast_ref::<io::Error>()
                .and_then(|io_e| io_e.raw_os_error())
            {
                code
            } else {
                1
            },
        );
    }
}
