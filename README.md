# blender-update

```
USAGE:
    blender-update [FLAGS] [OPTIONS] <PATH>

FLAGS:
    -f, --force      Skips the version check and forces installation.
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -p, --platform <platform>    The target platform (OS and arch). Defaults to the current platform.

                                 Accepted values are: [win32, win64, linux32, linux64, macos]
    -t, --type <ty>              The version/type of Blender to download. Defaults to official.

                                 Accepted values are: [official, 2.8]

ARGS:
    <PATH>    The Blender installation path.
```
